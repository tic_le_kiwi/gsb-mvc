﻿<?php
if (isset($_SESSION['status']))
{
  switch($_SESSION['status'])
  {
    case "visiteur": $hrefLogo="index.php?uc=accueil"; break;
    case "comptable": $hrefLogo="index.php?uc=synthese"; break;
    default: $hrefLogo="index.php?uc=accueil"; break;
  }
}
else
{
  $hrefLogo="index.php?uc=connexion";
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Intranet du Laboratoire Galaxy-Swiss Bourdin</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="./styles/styles.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" type="image/x-icon" href="./images/favicon.png" />
  </head>
  <body>
    <div id="page">
      <div id="entete">
        <div id="conteneur_logo">
          <a href="<?php echo $hrefLogo; ?>"> <img src="./images/logo.png" id="logoGSB" alt="Laboratoire Galaxy-Swiss Bourdin" title="Laboratoire Galaxy-Swiss Bourdin" /></a>
        </div>
        <p>Suivi du remboursement des frais</p>
      </div>