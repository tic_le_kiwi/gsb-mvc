<div id="contenu">
<?php
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
  $total = date('t');
  $jours = date('d');
  $mois = date('m');
  $temp_jour = $total - $jours; 
  switch ($mois)
  {
  case 01: $mois = "Janvier"; break;
  case 02: $mois = "Février"; break;
  case 03: $mois = "Mars"; break;
  case 04: $mois = "Avril"; break;
  case 05: $mois = "Mai"; break;
  case 06: $mois = "Juin"; break;
  case 07: $mois = "Juillet"; break;
  case 08: $mois = "Août"; break;
  case 09: $mois = "Septembre"; break;
  case 10: $mois = "Octobre"; break;
  case 11: $mois = "Novembre"; break;
  case 12: $mois = "Décembre"; break;
  }
  if($temp_jour != 0){
  	echo "<h2>Il vous reste plus que $temp_jour jour(s) avant la clôture de votre fiche de frais pour le mois de $mois.</h2>";	
  }else{
  	echo "<h2>Dernier jour avant la clôture du mois de $mois.</h2>";
  }
  
?>
</div>