﻿<div id="contenu">
  <form name="formValidFrais" method="post" action="index.php?uc=validerFrais&action=enregFrais">
    <h2> Validation des frais par visiteur </h2>
    <p><label>Visiteur :</label>
    <input type="text" id="lstVisiteur" name="lstVisiteur" value="<?php echo $nom; ?>"/></p>
    <p><label>Mois :</label> <input type="text" name="dateValid" id="dateValid"  size="12" value="<?php echo $mois; ?>"/></p>
    <h2>Frais au forfait </h2>
    <br>
    <table style="color:white;" border="1">
      <tr><th>Repas midi</th><th>Nuitée </th><th>Etape</th><th>Km </th><th>Situation</th></tr>
      <tr align="center">
        <td width="80" ><input type="text" size="3" name="repas" id="REP"  value='<?php echo $LesFicheFrais[3]['quantite']; ?>'/></td>
        <td width="80"><input type="text" size="3" name="nuitee" id="NUI" value='<?php echo $LesFicheFrais[2]['quantite']; ?>' /></td> 
        <td width="80"> <input type="text" size="3" name="etape" id="ETP" value='<?php echo $LesFicheFrais[0]['quantite']; ?>'/></td>
        <td width="80"> <input type="text" size="3" name="km" id="KM" value='<?php echo $LesFicheFrais[1]['quantite']; ?>'/></td>
        <td width="80"> 
          <?php 
          $opCR = $opVA = $opRB = $opCL = "";
          switch($LesInfosFicheFrais['idEtat']){
            case "CR": $opCR ="selected";
            case "VA": $opVA ="selected";
            case "RB": $opRB ="selected";
            case "CL": $opCL ="selected";
          }
          ?>
          <select size="3" name="situ" id="situ">
            <option value="CR" <?php echo $opCR; ?> >Enregistré</option>
            <option value="VA" <?php echo $opVA; ?> >Validé</option>
            <option value="RB" <?php echo $opRB; ?> >Remboursé</option>
            <option value="CL" <?php echo $opCL; ?> >Cloturé</option>
          </select></td>
        </tr>
    </table>
    <p class="titre" /><div style="clear:left;"><h2>Hors Forfait</h2></div>
    <?php
      for ($i=0; $i < count($LesFichesHorsForfait); $i++) { 
        $id = $LesFichesHorsForfait[$i]['id'];
        $date = $LesFichesHorsForfait[$i]['date'];
        $libelle = $LesFichesHorsForfait[$i]['libelle'];
        $montant = $LesFichesHorsForfait[$i]['montant'];
        $mois = $LesFichesHorsForfait[$i]['mois'];
        echo "  <table style='color:white;width:100%;'>
                  <tr>
                    <th>Date</th>
                    <th>Libellé </th>
                    <th>Montant</th>
                    <th>Mois</th>
                  </tr>
                  <tr>
                    <td>
                      <input type='text' size='12' name='hfDate$id' id='hfDate$id' value='$date'/>
                    </td>
                    <td>
                      <input type='text' size='10' name='hfLib$id' id='hfLib$id' value='$libelle'/>
                    </td> 
                    <td>
                      <input type='text' size='10' name='hfMont$id' id='hfMont$id' value='$montant'/>
                    </td>
                    <td>
                      <input type='text' size='10' name='hfMois$id' id='hfMois$id' value='$mois'/>
                    </td>
                  </tr>
              </table>";
      }

    ?>
    <br>
    <label>Nb Justificatifs</label>
    <input type="text" size="4" name="hcMontant" id="hcMontant" value="<?php echo $LesInfosFicheFrais['nbJustificatifs']; ?>"/>
    <div class="piedForm">
      <p>
        <input id="ajouter" type="submit" value="Mettre à jour" size="20" />
      </p> 
      </div>   
  </form>
</div>
