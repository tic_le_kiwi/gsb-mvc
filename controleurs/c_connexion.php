﻿<?php
if(!isset($_REQUEST['action'])){
	$_REQUEST['action'] = 'demandeConnexion';
}
$action = $_REQUEST['action'];
switch($action){
	case 'demandeConnexion':{
		include("vues/v_connexion.php");
		break;
	}
	case 'valideConnexion':{
		$login = $_REQUEST['login'];
		$mdp = md5($_REQUEST['mdp']);
		$visiteur = $pdo->getInfosVisiteur($login,$mdp);
		if(!is_array( $visiteur)){
			ajouterErreur("Login ou mot de passe incorrect.");
			include("vues/v_erreurs.php");
			include("vues/v_connexion.php");
		} else {
			$nbfiches = $pdo->getNbFicheFrais();
			$nbfichesVA = $pdo->getNbFicheFraisVA();
			$id = $visiteur['id'];
			$nom =  $visiteur['nom'];
			$prenom = $visiteur['prenom'];
			$status = $visiteur['status'];
			connecter($id,$nom,$prenom,$status);
			//creeNouvelleLFraisigne
			if($status == 'visiteur'){
				header('Location: index.php?uc=accueil');
			}else{
				header('Location: index.php?uc=synthese');
			}
			
		}
		break;
	}
	case 'deconnexion':{
		session_destroy();
	}
	default :{
		include("vues/v_connexion.php"); 		
		break;
	}
}
?>